# -*- coding: utf-8 -*-
import datetime
import time
from gluon import *
from gluon import current

if session.language:
    T.force(session.language)
else:
    T.force('hu')

now = Now = datetime.datetime.now()
CurrentDate = datetime.date(
    datetime.datetime.now().year,
    datetime.datetime.now().month,
    datetime.datetime.now().day)
CurrentDay = datetime.datetime.now().day
CurrentMonth = datetime.datetime.now().month
CurrentYear = datetime.datetime.now().year
OneHourEarly = (
    datetime.datetime.fromtimestamp(time.time()) -
    datetime.timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')

def date_widget(f, v):
    wrapper = DIV()
    inp = SQLFORM.widgets.string.widget(f, v, _class="jqdate")
    jqscr = SCRIPT("jQuery(document).ready(function(){jQuery('#%s').datepicker({dateFormat:'yy.mm.dd'});});" % inp['_id'], _type="text/javascript")
    wrapper.components.extend([inp, jqscr])
    return wrapper

app_db = dsscentral_db = DAL(settings.database_uri)

if auth.is_logged_in():
    Uid = auth.user.email
else:
    Uid = None

signature = app_db.Table(
    app_db,
    'signature',
    Field('created_on', 'datetime', default=request.now),
    Field('created_by', default=Uid),
    Field('modified_on', 'datetime', update=request.now),
    Field('modified_by', update=Uid))
signature.created_on.readable = signature.created_by.readable = signature.modified_by.readable = signature.modified_on.readable = False
signature.created_on.writable = signature.created_by.writable = signature.modified_by.writable = signature.modified_on.writable = False

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

# define table
SampleTable = app_db.define_table(
    'sample_table',
    Field('field_name', 'string', length=50),
    Field('url_name', 'string', length=50, widget=lambda field, value: SQLFORM.widgets.string.widget(field, value, _id='urlinput')),
    Field('description', 'text', comment="Description comment"),
    Field('serial', 'string', comment="Must be in format: 00123456"),
    Field('status', 'string', length=50, label=T("Status")),
    Field('field_list', 'list:string', length=50, label=T("Fields list")),
    Field('contact', 'string', length=50, comment="Email or distribution list."),
    Field('start_date', 'date'),
    Field('save_datetime', 'datetime'),
    Field('upload_document', 'upload', uploadfield='upload_document_data'),
    Field('upload_document_data', 'blob', default=''),
    Field('is_active', 'boolean', writable=False, readable=False, default=True),
    signature,
    format='%(id)s (%(field_name)s)')
# enable change tracking
SampleTable._enable_record_versioning(
    archive_db=app_db,
    archive_name='sample_table_archive',
    current_record='current_record',
    is_active='is_active')
# set rules
SampleTable.url_name.requires = IS_ALPHANUMERIC(error_message='must be alphanumeric!')
SampleTable.status.requires = IS_IN_SET(['active', 'inactive'])
SampleTable.status.default = 'active'
SampleTable.field_name.requires = IS_NOT_IN_DB(app_db, SampleTable.field_name)
SampleTable.description.requires = IS_NOT_EMPTY()
SampleTable.contact.requires = IS_EMPTY_OR(IS_EMAIL(error_message=T('Please insert valid email address!')))
SampleTable.serial.requires = [
    IS_NOT_IN_DB(app_db, SampleTable.serial),
    IS_MATCH(
        '^(00)(\d{6})$',
        error_message="Use the following format: 00123456")]


SampleLinkedTable = app_db.define_table(
    'sample_linked_table',
    Field('linked_entry_name', 'string'),
    Field('linked_to', app_db.sample_table),
    Field(
        'related_serials',
        'list:reference sample_table',
        comment=B(T("Please use the CTRL key to select more serials"))),
    # Field(
    #     'trainer_id',
    #     'list:integer',
    #     length=11,
    #     requires=IS_IN_DB(
    #         db(
    #             (db.auth_user.id == db.auth_membership.user_id) &
    #             (db.auth_membership.group_id == db.auth_group.id) &
    #             (db.auth_group.role == "dsstrainer")),
    #         db.auth_user.id,
    #         "%(last_name)s %(first_name)s",
    #         multiple=True),
    #     comment=B(T("Please use the CTRL key to select more trainer"))),
    signature
    )
SampleLinkedTable._enable_record_versioning(
    archive_db=app_db,
    archive_name='sample_linked_table_archive',
    current_record='current_record',
    is_active='is_active')
SampleLinkedTable.linked_to.requires = IS_IN_DB(app_db, app_db.sample_table.serial, zero="Please select serial")
# Teams.leader_id.requires = IS_EMPTY_OR(IS_IN_DB(dsscentral_db, Employees, "%(emp_name)s (%(id)s)"))

## after defining tables, uncomment below to enable auditing
auth.enable_record_versioning(db)
current.db = db
current.appdb = app_db

