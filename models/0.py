# coding: utf8
from gluon.storage import Storage
from gluon import current
from gluon.custom_import import track_changes; track_changes(True)
settings = Storage()

## DB settings
settings.database_uri = 'mysql://isdash:Passw0rd@localhost/web2py_boilerplate' # set db
settings.migrate = True

## Title settings
settings.title = 'Web2py Boilerplate'
settings.subtitle = 'Great stuff'
settings.brand = 'Boilerplate'
settings.version = '0.1'

## Mail settings
settings.email_server = '172.16.1.75:25'
settings.email_sender = 'DSSEPiCenter@t-systems.com'
settings.email_tls = False
settings.email_login = None

## Maintenance settings
settings.under_maintenance = False
settings.maintenance_message = XML("The website is temporally down due to undergoing maintenance work.<br>We apologize for the inconvenience this may cause you<br>and encourage you to check back with us again shortly.")
"""Example maintenance warning: XML("<h3>Attention</h3> <b>Next maintenance:</b> <i>2013.07.10</i>")"""
settings.maintenance_warning = ""

settings.test_emails = True
settings.ldap = False

# Test settings
settings.test_username = ""
settings.test_password = ""

current.settings = settings
